$(function () {

    var verifyFlag=false;

   $(".btn-reg").click(function () {
        var data={};
        var arr = $("#regForm").serializeArray();
       $.each(arr, function() {
           data[this.name] = this.value;
       });



       var nullFlag=false;

       $(".not-null-reg").each(function () {
          if($(this).val()===''){
              nullFlag=true;
              $(this).css("border-color","red");
              $(this).attr("placeholder","当前值不可为空!");
              return false;
          }
       });

       if ((!nullFlag)&& verifyFlag){

           $.ajax({
               type: 'POST',
               url: '/register',
               contentType: 'application/json', // 这句不加出现415错误:Unsupported Media Type
               data:JSON.stringify(data),
               success: function (data) {
                   if (data.success){
                       alert('注册成功!');
                       $('#myModal').modal('hide'); //隐藏模态框
                   }else {
                       alert("用户名已存在!")
                   }

               },
               error:function (err) {
                   alert(JSON.stringify(err));
               }
           });
       }

   });


    $('#verifyCode').slideVerify({
        type : 2,		//类型
        vOffset : 5,	//误差量，根据需求自行调整
        vSpace : 5,	//间隔
        imgName : ['1.jpg', '2.jpg','3.jpg','4.jpg','5.jpg','6.jpg','7.jpg'],
        imgSize : {
            width: '308px',
            height: '150px'
        },
        blockSize : {
            width: '40px',
            height: '40px'
        },
        barSize : {
            width : '318px',
            height : '40px'
        },
        ready : function() {

        },
        success : function() {
            verifyFlag=true;

        },
        error : function() {

        }

    });

   $(".not-null").change(function () {
       if($(this).val()!==''){
           $(this).css('border-color','#ccc');
       }
   });

   $("#pwd").change(function () {
       $("#repwd").change();
   });

   $("#repwd").change(function () {
       var pwd = $("#pwd");
       if(pwd.val()!==$(this).val()){
           $(this).css("border-color","red");
           $(".pwdErr").removeClass('hidden');
           $(".btn-reg").attr('disabled',true);
       }else {
           $(this).css("border-color","#ccc");
           $(".pwdErr").addClass('hidden');
           $(".btn-reg").attr('disabled',false);
       }
   });

   $('#password').keypress(function (event) {
       if(event.keyCode===13){
           $(".btn-login").click();
       }

   });

   $(".btn-login").click(function () {
       var fm = $("#loginForm");
       var data={};
       var arr = fm.serializeArray();
       $.each(arr, function() {
           data[this.name] = this.value;
       });
       var nullFlag=false;

       $(".not-null-login").each(function () {
           if($(this).val()===''){
               nullFlag=true;
               $(this).css("border-color","red");
               $(this).attr("placeholder","当前值不可为空!");
               return false;
           }
       });

       if(!nullFlag){
           $.ajax({
               type: 'POST',
               url: '/login',
               contentType: 'application/json', // 这句不加出现415错误:Unsupported Media Type
               data:JSON.stringify(data),
               success: function (data) {
                   if(data==="index"){
                       window.location.href="/index";
                   }else {
                       $("#tip").html(data);
                   }
               }
           });
       }

   });

    $(".btn-findPwd").click(function () {
        var obj1 = $("#userName");
        var obj2 = $("#mail");
        $(".not-null-find").each(function () {
            if($(this).val()===''){
                nullFlag=true;
                $(this).css("border-color","red");
                $(this).attr("placeholder","当前值不可为空!");
                return false;
            }
        });

        var data = {};
        data.userName = obj1.val();
        data.mail = obj2.val();

        obj1.change(function () {
           $(".usrErr").addClass("hidden");
        });
        obj2.change(function () {
            $(".mailErr").addClass("hidden");
        });

        $.ajax({
            type: 'POST',
            url: '/findPwd',
            contentType: 'application/json', // 这句不加出现415错误:Unsupported Media Type
            data:JSON.stringify(data),
            success: function (data) {
                if(data.success){
                    alert("请求已发送，您将在邮件中找到您的密码!")
                }else {
                    $("."+data.position).removeClass("hidden");
                }
            },
            error:function (err) {
                alert(JSON.stringify(err));
            }
        });


    });


});