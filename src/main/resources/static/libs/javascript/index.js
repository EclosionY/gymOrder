$(function () {



    initHead();

    requiredPage('common/leftNav', '#leftNav', function () {
        $('.toMsg').click(function () {
            initMsg();
        });

        $('#toBook').click(function () {
            initBook();
        });

        $('#myInfo').click(function () {
            initMyInfo();
        });

        $('.myBook').click(function () {
            initMyBook();
        });

        $('#changePwd').click(function () {
            initChangePwd();
        });

        $("#toMailCheck").click(function () {
            initMailCheck();
        });

        $(".bookedList").click(function () {
            initBookedList()
        });

        $(".gymList").click(function () {
            initGymList()
        });

        $(".messageList").click(function () {
            initMessageList()
        });

        $(".reqList").click(function () {
            initReqList()
        });

        $(".typeList").click(function () {
            initTypeList()
        });

        $(".userList").click(function () {
            initUserList()
        })

    });


    function initChooseTime(reData,subTimes,timeStr) {
        if(JSON.stringify(reData.gym)==="{}"){
            alert("请先选择场地!");
            return false;
        }
        var nowDate = new Date();
        var nowDay = nowDate.getFullYear()+'-'+(nowDate.getMonth()+1)+'-'+nowDate.getDate();
        $.ajax({
            type: 'POST',
            url: '/initBooked',
            contentType: 'application/json', // 这句不加出现415错误:Unsupported Media Type
            data:JSON.stringify(reData),
            success: function (data) {
                $('#chooseTimes').html(data);
                var nowHour = nowDate.getHours();
                var chooseBolock = $(".chooseBlock");
                if(nowDay === reData.orderDay){
                    chooseBolock.each(function (i,el) {
                        if(i<(nowHour-7)){
                            $(el).addClass("myDisabled")
                        }
                    });
                }

                chooseBolock.click(function () {
                    if ($(this).hasClass('used')||$(this).hasClass('myDisabled')) {
                        return false;
                    }


                    if ($(this).hasClass("choosed")) {
                        timeStr = timeStr.replace(',' + $(this).text(), '');
                    } else {
                        if(subTimes.length>=3){
                            alert("最多选择3个时段!");
                            return false;
                        }
                        timeStr === ',' ? timeStr = timeStr + $(this).text() : timeStr = timeStr + ',' + $(this).text();
                    }

                    $(this).toggleClass("choosed");
                    var times = timeStr.split(',');
                    times.splice(0, 1);
                    subTimes = times

                }); //获取选择的时段，处理完成选择的时段。


                $(".book").click(function () {
                    if(JSON.stringify(subTimes)==="{}"){
                        alert("请选择预约时间段!");
                        return false;
                    }

                    reData.orderTimes = subTimes.join(',');
                    $.ajax({
                        type: 'POST',
                        url: '/addGymOrder',
                        contentType: 'application/json', // 这句不加出现415错误:Unsupported Media Type
                        data:JSON.stringify(reData),
                        success: function (data) {
                            if(data){
                                alert("您已预约成功!");
                                initBook();
                            }
                        },
                        error:function (err) {
                            alert(JSON.stringify(err));
                        }
                    });
                });
            },
            error:function (err) {
                alert(JSON.stringify(err));
            }
        });

    }


    function initBook() {
        var subDate = '';
        var subTimes = {};
        var timeStr = ',';
        var gymID;
        var bookedNum;

        loadPage('/getGyms', '#mainPanel', function () {
            $("#bookNum").change(function () {
                bookedNum = $(this).val();
                data.bookedNum = bookedNum;
                initChooseTime(data,subTimes,timeStr);
            });

            var data = {};
            data.gym = {};

            laydate.render({
                elem: '#selectDate'
                ,min: 0
                ,max: 7
                , position: 'static'
                , theme: 'grid'
                , showBottom: false
                , change: function (value, date) {
                    subDate = subDate = date.year + '-' + date.month + '-' + date.date;;
                    data.orderDay = subDate;
                    initChooseTime(data,subTimes,timeStr);

                }
                , ready: function (date) {
                    subDate = date.year + '-' + date.month + '-' + date.date;
                    data.orderDay = subDate;
                }
            });

            $(".gyms").click(function () {
                gymID = $(this).data("gid");
                data.gym.gymID = gymID;
                $(".gyms").removeClass("selected");
                $(this).addClass("selected");
            });


        });
    }

    function initMsg(pageSize) {
        if(pageSize==null){pageSize=10;}
        $.ajax({
            type: 'get',
            url: '/findMessagePage?pageNum=0&pageSize='+pageSize+"&type=0",
            contentType: 'application/json', // 这句不加出现415错误:Unsupported Media Type
            data:"",
            success: function (data) {
                $("#mainPanel").html(data);
                var mid = "";

                $(".reply-btn").click(function () {
                    mid = $(this).data("mid");
                    $("#m-"+mid).toggle();
                });

                $(".send").click(function () {
                    var content = $("#c-"+mid).val();
                    var data={};
                    data.remark = mid;
                    data.content = content;
                    $.ajax({
                        type: 'POST',
                        url: '/addReply',
                        contentType: 'application/json', // 这句不加出现415错误:Unsupported Media Type
                        data:JSON.stringify(data),
                        success: function (data) {
                            initMsg();
                        },
                        error:function (err) {
                            alert(JSON.stringify(data));
                        }
                    });
                });

                $("#comment").click(function () {
                    var data = {};
                    var content = $("#content").val();
                    if(content.trim()===''){
                        return false;
                    }
                    data.content = content;
                    $.ajax({
                        type: 'POST',
                        url: '/addMessage',
                        contentType: 'application/json', // 这句不加出现415错误:Unsupported Media Type
                        data:JSON.stringify(data),
                        success: function (data) {
                           initMsg();
                        },
                        error:function (err) {
                            alert(JSON.stringify(data));
                        }
                    });

                });


                $(".btn-more").click(function () {
                    initMsg(pageSize*1+5)
                });

                $("#reload").click(function () {
                    initMsg();
                });

            },
            error:function (err) {
                alert(JSON.stringify(data));
            }
        });

    }

    function initMyInfo() {
        requiredPage("/personCenter/myInfo","#mainPanel",function () {
            $("#link-changeHeadImg").click(function () {
                    $(".fileUpload").toggle();
            });

            //比较简洁，细节可自行完善
            $('#uploadSubmit').click(function () {
                var data = new FormData($('#uploadForm')[0]);
                $.ajax({
                    url: '/changeHeadPath',
                    type: 'POST',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        initHead();
                        initMyInfo();
                    },
                    error: function (err) {
                        alert(JSON.stringify(err));
                    }
                });
            });

            $("#saveChange").click(function () {
                var data={};
                var arr = $("#form-info").serializeArray();
                $.each(arr, function() {
                    data[this.name] = this.value;
                });
                $.ajax({
                    type: 'POST',
                    url: '/changeInfo',
                    contentType: 'application/json', // 这句不加出现415错误:Unsupported Media Type
                    data:JSON.stringify(data),
                    success: function (data) {
                        alert("保存成功!");
                        initMyInfo();
                        initHead();
                    },
                    error:function (err) {
                        alert(JSON.stringify(err));
                    }
                });

            });
        })

    }

    function initMyBook(pageSize,pageNum) {
        pageSize = 10;
        if(pageNum==null){pageNum = 0;}
        loadPage("/findMyOrders?pageNum="+pageNum+"&pageSize="+pageSize,"#mainPanel",function () {
            $(".cancelBook").click(function () {
                if (window.confirm("取消后不可恢复,确定取消预约吗？")){
                    data = {};
                    data.orderID = $(this).data("oid");
                    $.ajax({
                        type: 'DELETE',
                        url: '/cancelBook',
                        contentType: 'application/json', // 这句不加出现415错误:Unsupported Media Type
                        data:JSON.stringify(data),
                        success: function (data) {
                            if(data){
                                initMyBook();
                            }else {
                                alert("取消失败，请联系管理员!")
                            }

                        },
                        error:function (err) {
                            alert(JSON.stringify(err));
                        }
                    });
                }

            });


            $(".prePage").click(function () {
                initMyBook(10,($(this).data("page")*1-1));
            });

            $(".nextPage").click(function () {
                initMyBook(10,($(this).data("page")*1+1));
            });

            $(".pageNum").click(function () {
                initMyBook(10,($(this).html()*1-1));
            });

        })
    }

    function initChangePwd() {
        requiredPage("/personCenter/changePwd","#mainPanel",function () {

            $("#newPwd").change(function () {
                $("#rePwd").change();
            });

            $("#rePwd").change(function () {
                var pwd = $("#newPwd");
                if(pwd.val()!==$(this).val()){
                    $(this).css("border-color","red");
                    $(".pwdErr").removeClass('hidden');
                    $(".confirmChange").attr('disabled',true);
                }else {
                    $(this).css("border-color","#ccc");
                    $(".pwdErr").addClass('hidden');
                    $(".confirmChange").attr('disabled',false);
                }
            });

            $(".not-null").change(function () {
                if($(this).val()!==''){
                    $(this).css('border-color','#ccc');
                }
            });

            $(".confirmChange").click(function () {

                $(".not-null").each(function () {
                    if($(this).val()===''){
                        nullFlag=true;
                        $(this).css("border-color","red");
                        $(this).attr("placeholder","当前值不可为空!");
                        return false;
                    }
                });

                var oldPwd = $("#oldPwd").val();
                var newPwd = $("#newPwd").val();

                var data={};
                data.pwd = oldPwd;
                data.remark = newPwd;

                $.ajax({
                    type: 'POST',
                    url: '/changePwd',
                    contentType: 'application/json', // 这句不加出现415错误:Unsupported Media Type
                    data:JSON.stringify(data),
                    success: function (data) {
                        if(data){alert("密码修改成功，将在下次登录时生效!")}else {
                           alert("密码修改失败，原密码输入错误!");
                        }
                    },
                    error:function (err) {
                        alert(JSON.stringify(err));
                    }
                });


            });
        })
    }

    function initHead() {
        requiredPage("/common/head","#head",function () {
            $(".myInfo").click(function () {
                initMyInfo();
            });

            $(".bookHis").click(function () {
                initMyBook();
            });

            $(".changePwd").click(function () {
                initChangePwd();
            });
        })
    }

    function initMailCheck() {
        requiredPage("/personCenter/mailCheck","#mainPanel",function () {

            if($("#usrMail").text()==="(未验证)"){
                $("#usrMail").text("邮箱信息为空,暂不可用");
                return false;
            }

            var countdown=60;
            function settime(obj) { //发送验证码倒计时
                if (countdown === 0) {
                    obj.attr('disabled',false);
                    //obj.removeattr("disabled");
                    obj.text("免费获取验证码");
                    countdown = 60;
                    return;
                } else {
                    obj.attr('disabled',true);
                    obj.text("重新发送(" + countdown + ")");
                    countdown--;
                }
                setTimeout(function() {
                        settime(obj) }
                    ,1000)
            }


            $("#sendMail").click(function () {
                settime($(this));
                $.ajax({
                    type: 'POST',
                    url: '/mailSender',
                    contentType: 'application/json', // 这句不加出现415错误:Unsupported Media Type
                    data:"",
                    success: function (data) {

                    },
                    error:function (err) {
                        alert(JSON.stringify(err));
                    }
                });
            });

            $("#subCode").click(function () {

                var data = {};
                var uCode = $("#uCode").val();
                if (uCode.trim() === ""){return false;}
                data.code = uCode;
                $.ajax({
                    type: 'POST',
                    url: '/mailCheck',
                    contentType: 'application/json', // 这句不加出现415错误:Unsupported Media Type
                    data:JSON.stringify(data),
                    success: function (data) {
                        if(data){
                            initMailCheck();
                        }else {
                            alert("验证码错误!");
                        }
                    },
                    error:function (err) {
                        alert(JSON.stringify(err));
                    }
                });
            });

        });
    }

    function initBookedList(pageSize,pageNum) {
        pageSize = 10;
        if(pageNum==null){pageNum = 0;}
        loadPage("/findAllOrders?pageNum="+pageNum+"&pageSize="+pageSize,"#mainPanel",function () {

            $(".prePage").click(function () {
                initBookedList(10,($(this).data("page")*1-1));
            });

            $(".nextPage").click(function () {
                initBookedList(10,($(this).data("page")*1+1));
            });

            $(".pageNum").click(function () {
                initBookedList(10,($(this).html()*1-1));
            });

        })
    }

    function initGymList(pageSize,pageNum) {
        pageSize = 10;
        if(pageNum==null){pageNum = 0;}
        loadPage("/findGymPage?pageNum="+pageNum+"&pageSize="+pageSize,"#mainPanel",function () {

            $(".btn-save").click(function () {
                var myForm;
                myForm = new FormData(document.getElementById("gymForm"));

                $.ajax({
                    url : '/addGym',
                    type : 'POST',
                    data : myForm,
                    async : false,
                    cache : false,
                    contentType : false,// 告诉jQuery不要去设置Content-Type请求头
                    processData : false,// 告诉jQuery不要去处理发送的数据
                    success : function(data) {
                        if(data.resMap.success){
                            var myModel =  $('#myModal');
                           myModel.on('hidden.bs.modal', function () {
                                initGymList();
                            });
                            alert("保存成功!");
                            myModel.modal("hide");

                        }else {
                           alert("保存失败!")
                        }
                    },
                    error : function(data) {
                        alert(JSON.stringify(data));
                    }
                });

            });

            $(".prePage").click(function () {
                initGymList(10,($(this).data("page")*1-1));
            });

            $(".nextPage").click(function () {
                initGymList(10,($(this).data("page")*1+1));
            });

            $(".pageNum").click(function () {
                initGymList(10,($(this).html()*1-1));
            });

            $(".delete").click(function () {
                var data={};
                data.gymID = $(this).data("gid");
                if(confirm("删除之后不可恢复,确认删除吗?")){
                    $.ajax({
                        type: 'DELETE',
                        url: '/deleteGym',
                        contentType: 'application/json', // 这句不加出现415错误:Unsupported Media Type
                        data:JSON.stringify(data),
                        success: function (data) {
                            if(data.success){
                                initGymList();
                            }

                        },
                        error:function (err) {
                            alert(JSON.stringify(err));
                        }
                    });
                }

            });

            $(".viewInfo").click(function () {
                var me = this;
                var gid = $(this).data('gid');
                $.ajax({
                    type: 'get',
                    url: '/findGymByID?gymID='+gid,
                    contentType: 'application/json', // 这句不加出现415错误:Unsupported Media Type
                    success: function (data) {
                            $("#gymInfo").html(data);
                            $("#myModal2").modal("show");
                    },
                    error:function (err) {
                        alert(JSON.stringify(err));
                    }
                });
            });

            $(".btn-change").click(function () {
                var myForm;
                myForm = new FormData(document.getElementById("checkGymForm"));
                myForm.append("gymID",$("#checkGymForm").data("gid"));

                $.ajax({
                    url : '/addGym',
                    type : 'POST',
                    data : myForm,
                    async : false,
                    cache : false,
                    contentType : false,// 告诉jQuery不要去设置Content-Type请求头
                    processData : false,// 告诉jQuery不要去处理发送的数据
                    success : function(data) {
                        if(data.resMap.success){
                            var myModel =  $('#myModal2');
                            myModel.on('hidden.bs.modal', function () {
                                initGymList();
                            });
                            alert("保存成功!");
                            myModel.modal("hide");

                        }else {
                            alert("保存失败!")
                        }
                    },
                    error : function(data) {
                        alert(JSON.stringify(data));
                    }
                });

            });

        }
)
    }

    function initMessageList(pageSize,pageNum) {
        pageSize = 10;
        if(pageNum==null){pageNum = 0;}
        loadPage("/findMessagePage?pageNum="+pageNum+"&pageSize="+pageSize+"&type=1","#mainPanel",function () {

            $(".delete").click(function () {
                if (window.confirm("删除后不可恢复,确定删除该留言吗？")){
                    data = {};
                    data.messageID = $(this).data("mid");
                    $.ajax({
                        type: 'DELETE',
                        url: '/deleteMsg',
                        contentType: 'application/json', // 这句不加出现415错误:Unsupported Media Type
                        data:JSON.stringify(data),
                        success: function (data) {
                            if(data){
                                initMessageList();
                            }else {
                                alert("删除失败");
                            }

                        },
                        error:function (err) {
                            alert(JSON.stringify(err));
                        }
                    });
                }

            });

            $(".prePage").click(function () {
                initMessageList(10,($(this).data("page")*1-1));
            });

            $(".nextPage").click(function () {
                initMessageList(10,($(this).data("page")*1+1));
            });

            $(".pageNum").click(function () {
                initMessageList(10,($(this).html()*1-1));
            });

        })
    }

    function initReqList() {
        requiredPage("/adminCenter/reqList","#mainPanel",function () {

        })
    }

    function initTypeList() {
        loadPage("/findGymTypePage?pageNum=0&pageSize=10","#mainPanel",function () {
            var newLineID = 0;

            $(".viewInfo").click(function () {
                $('#myModal').modal('toggle')
            });

            $(".btn-addLine").click(function () {
                $("#typeTable").find("tbody").prepend('<tr>\n' +
                    '                    <td><input type="text" style="width: 80px" id="n-'+newLineID+'"></td>\n' +
                    '                    <td>'+$("#userNickName").text()+'</td>\n' +
                    '                    <td>-</td>\n' +
                    '                    <td><input type="text" style="width: 80px" id="r-'+newLineID+'"></td>\n' +
                    '                    <td>-</td>\n' +
                    '                    <td>\n' +
                    '                        <a href="#"><span class="glyphicon glyphicon-floppy-disk save" data-lid="'+newLineID+'"></span></a>&nbsp;&nbsp;&nbsp;\n' +
                    '                        <a href="#"><span class="glyphicon glyphicon-remove text-danger cancel"></span> </a>\n' +
                    '                    </td>\n' +
                    '                </tr>');


                $(".save").click(function () {
                    var data = {};
                    data.typeName = $("#n-"+$(this).data("lid")).val();
                    data.remark = $("#r-"+$(this).data("lid")).val();
                    data.createUser = $("#userNickName").text();
                        $.ajax({
                            type: 'POST',
                            url: '/addGymType',
                            contentType: 'application/json', // 这句不加出现415错误:Unsupported Media Type
                            data:JSON.stringify(data),
                            success: function (data) {
                                if(data.success){
                                    alert("保存成功!");
                                    initTypeList();
                                }

                            },
                            error:function (err) {
                                alert(JSON.stringify(err));
                            }
                        });
                    });

                newLineID++;

                $(".cancel").click(function () {
                    initTypeList();
                });
            });


            $(".delete").click(function () {
                var data={};
                data.typeID = $(this).data("tid");
                $.ajax({
                    type: 'POST',
                    url: '/deleteGymType',
                    contentType: 'application/json', // 这句不加出现415错误:Unsupported Media Type
                    data:JSON.stringify(data),
                    success: function (data) {
                        if(data.success){
                            initTypeList();
                        }

                    },
                    error:function (err) {
                        alert(JSON.stringify(err));
                    }
                });
            });

        })
    }

    function initUserList(pageSize,pageNum) {
        pageSize = 10;
        if(pageNum==null){pageNum = 0;}
        loadPage("/findUserPage?pageNum="+pageNum+"&pageSize="+pageSize,"#mainPanel",function () {

            $(".prePage").click(function () {
                initUserList(10,($(this).data("page")*1-1));
            });

            $(".nextPage").click(function () {
                initUserList(10,($(this).data("page")*1+1));
            });

            $(".pageNum").click(function () {
                initUserList(10,($(this).html()*1-1));
            });

        })
    }

});
