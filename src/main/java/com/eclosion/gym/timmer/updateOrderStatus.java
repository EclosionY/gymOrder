package com.eclosion.gym.timmer;

import com.eclosion.gym.model.entity.Gym;
import com.eclosion.gym.model.entity.GymBookOrder;
import com.eclosion.gym.service.GymOrderService;
import com.eclosion.gym.service.GymService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Configuration          //证明这个类是一个配置文件
@EnableScheduling       //打开quartz定时器总开关
public class updateOrderStatus {

    @Autowired
    private GymOrderService gymOrderService;
    @Autowired
    private GymService gymService;

    @Scheduled(cron = "0 0 0/1 * * ?")
    public void timer() throws ParseException {
        System.out.println("已完成预约自动同步......");
        //获取当前时间
        List<GymBookOrder> gymBookOrders = gymOrderService.finGymBookOrders(0);
        Gym gym=null;
        for (GymBookOrder gymBookOrder :gymBookOrders){
            String day = gymBookOrder.getOrderDay();
            String[] times =gymBookOrder.getOrderTimes().split(",");
            String timeRange = times[times.length-1];
            String time = timeRange.split("-")[1]+":00";
            String dateTime = day+" "+time;
            SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = null;
            date=formatter.parse(dateTime);
            Date nowDate = new Date();
            if(nowDate.after(date)|| nowDate.equals(date)){
                gym = gymBookOrder.getGym();
                gym.setBookedNum(gym.getBookedNum()+gymBookOrder.getBookedNum());
                gymService.addGym(gym);
                gymBookOrder.setStatus(1);
                gymOrderService.addGymOrder(gymBookOrder);
            }
        }
    }
}
