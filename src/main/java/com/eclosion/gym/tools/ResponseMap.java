package com.eclosion.gym.tools;

import lombok.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/27
 * Time:16:06
 */

@Getter
@Setter

@ToString
public class ResponseMap {
    private Map<String,Object> resMap;

    public ResponseMap(){
        resMap = new HashMap<>();
        resMap.put("success",false);
    }

    public Map ok(){
        resMap.put("success",true);
        return resMap;
    }

    public Map ok(Object obj){
        resMap.put("success",true);
        resMap.put("content",obj);
        return resMap;
    }

    public Map err(String err){
        resMap.put("success",false);
        resMap.put("err",err);
        return resMap;
    }

}
