package com.eclosion.gym.tools;

import java.util.UUID;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/19
 * Time:17:27
 */
public  class CreateUUID {
    public static String getUUID(){
        UUID uuid=UUID.randomUUID();
        String str = uuid.toString();
        return str.replace("-", "");
    }

    public static String getShortUUID(){
        UUID uuid=UUID.randomUUID();
        String str = uuid.toString();
        return str.split("-")[0];
    }
}
