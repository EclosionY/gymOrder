package com.eclosion.gym.tools;

import java.io.File;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/22
 * Time:11:24
 */
public class GetClassDir {
    public static String getParentPath(){
        String path = System.getProperty("java.class.path");
        int firstIndex = path.lastIndexOf(System.getProperty("path.separator")) + 1;
        int lastIndex = path.lastIndexOf(File.separator) + 1;
        return path.substring(firstIndex, lastIndex);
    }
}
