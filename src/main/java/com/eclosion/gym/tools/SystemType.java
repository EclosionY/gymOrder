package com.eclosion.gym.tools;

import java.util.Properties;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/22
 * Time:12:05
 */
public class SystemType {

    public static boolean isWin() {
        Properties prop = System.getProperties();

        String os = prop.getProperty("os.name");

        if (os != null && os.toLowerCase().indexOf("linux") > -1) {
            return false;
        } else {
            return true;
        }
    }

}
