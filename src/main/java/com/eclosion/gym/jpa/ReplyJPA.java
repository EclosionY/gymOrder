package com.eclosion.gym.jpa;

import com.eclosion.gym.model.entity.Reply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.io.Serializable;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/13
 * Time:15:12
 */
public interface ReplyJPA extends JpaRepository<Reply,String>,JpaSpecificationExecutor<Reply>,Serializable{
    Reply findByReplyID(String messageID);
}
