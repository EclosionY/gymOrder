package com.eclosion.gym.jpa;

import com.eclosion.gym.model.entity.GymType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.io.Serializable;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/27
 * Time:14:27
 */
public interface GymTypeJPA extends JpaRepository<GymType,String>,JpaSpecificationExecutor<GymType>,Serializable {

}
