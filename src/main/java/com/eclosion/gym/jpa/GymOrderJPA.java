package com.eclosion.gym.jpa;

import com.eclosion.gym.model.entity.Gym;
import com.eclosion.gym.model.entity.GymBookOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.io.Serializable;
import java.util.List;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/27
 * Time:14:28
 */
public interface GymOrderJPA extends JpaRepository<GymBookOrder,String>,JpaSpecificationExecutor<GymBookOrder>,Serializable {
    List<GymBookOrder> findByStatus(int status);
    List<GymBookOrder> findByOrderDayAndStatusAndGym(String orderDay, int status, Gym gym);
}
