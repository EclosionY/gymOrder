package com.eclosion.gym.jpa;

import com.eclosion.gym.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.io.Serializable;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/13
 * Time:15:12
 */
public interface UserJPA extends JpaRepository<User,String>,JpaSpecificationExecutor<User>,Serializable{
   User findByUserNameAndPwd(String uName, String pwd);

   User findByUserName(String uName);

   User findByMail(String mail);
}
