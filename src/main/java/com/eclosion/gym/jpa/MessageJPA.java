package com.eclosion.gym.jpa;

import com.eclosion.gym.model.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.io.Serializable;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/13
 * Time:15:12
 */
public interface MessageJPA extends JpaRepository<Message,String>,JpaSpecificationExecutor<Message>,Serializable{
    Message findByMessageID(String messageID);
}
