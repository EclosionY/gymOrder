package com.eclosion.gym.jpa;

import com.eclosion.gym.model.entity.Gym;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.io.Serializable;
import java.util.List;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/27
 * Time:14:28
 */
public interface GymJPA extends JpaRepository<Gym,String>,JpaSpecificationExecutor<Gym>,Serializable {
     Gym findByGymID(String gymID);
}
