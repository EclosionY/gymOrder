package com.eclosion.gym.service;

import com.eclosion.gym.jpa.GymSecondTypeJPA;
import com.eclosion.gym.jpa.GymTypeJPA;
import com.eclosion.gym.model.entity.GymSecondType;
import com.eclosion.gym.model.entity.GymType;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/27
 * Time:14:31
 */

@Getter
@Setter
@ToString
@Service
public class GymTypeService {
    private final GymTypeJPA gymTypeJPA;

    private final GymSecondTypeJPA gymSecondTypeJPA;

    @Autowired
    public GymTypeService(GymTypeJPA gymTypeJPA, GymSecondTypeJPA gymSecondTypeJPA) {
        this.gymTypeJPA = gymTypeJPA;
        this.gymSecondTypeJPA = gymSecondTypeJPA;
    }

    public Page<GymType> findGymTypeCriteria(Integer page, Integer size, final GymType gymType) {
        Pageable pageable = new PageRequest(page, size, Sort.Direction.DESC, "createTime");
        Page<GymType> gymTypePage = gymTypeJPA.findAll(new Specification<GymType>(){
            @Override
            public Predicate toPredicate(Root<GymType> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                Predicate p1 = criteriaBuilder.equal(root.get("createUser").as(String.class), gymType.getCreateUser());
                Predicate p2 = criteriaBuilder.equal(root.get("typeName").as(String.class), gymType.getTypeName());
                Predicate p3 = criteriaBuilder.equal(root.get("status").as(String.class), gymType.getStatus());
                query.where(criteriaBuilder.and(p1,p2,p3));
                return query.getRestriction();
            }
        },pageable);
        return gymTypePage;
    }

    public Page<GymType> findGymTypePage(int page,int size){
        Sort sort = new Sort(Sort.Direction.DESC,"createTime");
        Pageable pageable = new PageRequest(page,size,sort);
        return gymTypeJPA.findAll(pageable);
    }

    public Boolean addGymType(GymType gymType){
        try {
            gymTypeJPA.save(gymType);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Boolean addGymSecondType(GymSecondType gymSecondType){
        try {
            gymSecondTypeJPA.save(gymSecondType);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public List<GymSecondType> findGymSecondTypes(GymType gymType){
        return gymType.getGymSecondTypes();
    }

    public void deleteGymType(GymType type){
        gymTypeJPA.delete(type);
    }

}
