package com.eclosion.gym.service;

import com.eclosion.gym.jpa.GymOrderJPA;
import com.eclosion.gym.model.entity.Gym;
import com.eclosion.gym.model.entity.GymBookOrder;
import com.eclosion.gym.model.entity.User;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.*;
import java.util.List;


/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/27
 * Time:14:32
 */

@Getter
@Setter
@ToString
@Service
public class GymOrderService {
    private final GymOrderJPA gymOrderJPA;

    @Autowired
    public GymOrderService(GymOrderJPA gymOrderJPA) {
        this.gymOrderJPA = gymOrderJPA;
    }

    public boolean addGymOrder(GymBookOrder gymBookOrder){
        gymOrderJPA.save(gymBookOrder);
        return true;
    }

    //status 为3时查询全部
    public Page<GymBookOrder> findMyBookOrder(User user, int page, int size,int status) {
        Specification<GymBookOrder> specification = new Specification<GymBookOrder>() {
            @Override
            public Predicate toPredicate(Root<GymBookOrder> root,
                                         CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                Predicate predicate;
                Path<User> $user = root.get("user");
                Path<Integer> $status = root.get("status");
                Predicate _user = criteriaBuilder.equal($user,user);
                predicate = criteriaBuilder.and(_user);
                if(status != 3){
                    Predicate _status = criteriaBuilder.equal($status,status);
                    predicate = criteriaBuilder.and(_user,_status);
                }
                return predicate;
            }
        };
        Sort sort = new Sort(Sort.Direction.DESC,"createTime");
        Pageable pageable = new PageRequest(page,size,sort);
        return gymOrderJPA.findAll(specification, pageable);
    }

    public Page<GymBookOrder> findAllBookOrder(int page,int size){
        Sort sort = new Sort(Sort.Direction.DESC,"createTime");
        Pageable pageable = new PageRequest(page,size,sort);
        return gymOrderJPA.findAll(pageable);
    }

    public List<GymBookOrder> finGymBookOrders(int status){
        return gymOrderJPA.findByStatus(status);
    }
    public List<GymBookOrder> findGymOrderByDayAndStatusAndGym(String orderDay,int status,Gym gym){return gymOrderJPA.findByOrderDayAndStatusAndGym(orderDay,status,gym);}
    public boolean deleteGymOrder(GymBookOrder gymBookOrder){
        gymOrderJPA.delete(gymBookOrder);
        return true;
    }
}
