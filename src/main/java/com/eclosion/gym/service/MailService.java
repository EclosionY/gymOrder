package com.eclosion.gym.service;

import com.eclosion.gym.tools.CreateUUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/21
 * Time:10:20
 */

@Service
public class MailService {
    @Autowired
    private JavaMailSender mailSender; //自动注入的Bean

    @Value("${spring.mail.username}")
    private String Sender; //读取配置文件中的参数

    public void sendSimpleMail() throws Exception {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(Sender);
        message.setTo("a15087129834@163.com"); //自己给自己发送邮件
        message.setSubject("主题：简单邮件");
        message.setText("测试邮件内容");
        mailSender.send(message);
    }

    public String sendCodeMail(String target) throws Exception {
        String uCode="";
        MimeMessage message = null;
        try {
            message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(Sender);
            helper.setTo(target);
            helper.setSubject("东华理工大学体育馆预约系统邮箱验证");

            StringBuilder sb = new StringBuilder();
            uCode = CreateUUID.getShortUUID();
            sb.append("<div style=\"border:  #E38D13 dashed;background: #31B0D5; padding:50px;\"><h1 style=\"color: white; text-align:center \">东华理工大学体育馆预约系统邮箱验证</h1><hr /><h3 style='text-align:center'>您的验证码为:<span style='color:red'>").append(uCode).append("</span></h3><p style=\"text-align: center;\">在输入框中填入验证码即可完成验证</p></div>");
            helper.setText(sb.toString(), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert message != null;
        mailSender.send(message);
        return uCode;
    }

    public Boolean sendPwdMail(String target,String userName,String pwd) throws Exception {
        MimeMessage message = null;
        try {
            message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom(Sender);
            helper.setTo(target);
            helper.setSubject("东华理工大学体育馆预约系统密码找回");
            helper.setText("<div style=\"border:  #E38D13 dashed;background: #31B0D5; padding:50px;\"><h1 style=\"color: white; text-align:center \">东华理工大学体育馆预约系统密码找回</h1><hr /><h3 style='text-align:center'>尊敬的用户" + userName + ",您的密码码为:<span style='color:red'>" + pwd + "</span></h3><p style=\"text-align: center;\">可直接登录</p></div>", true);
            mailSender.send(message);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
