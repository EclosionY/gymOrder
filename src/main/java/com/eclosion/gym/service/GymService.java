package com.eclosion.gym.service;

import com.eclosion.gym.jpa.GymJPA;
import com.eclosion.gym.model.entity.Gym;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/27
 * Time:14:31
 */

@Getter
@Setter
@ToString
@Service
public class GymService {

    @Autowired
    private GymJPA gymJPA;

    public boolean addGym(Gym gym){
        try {
            gymJPA.save(gym);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

    }

    public List<Gym> findGymList(){
        try {
            return gymJPA.findAll();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Page<Gym> findGymPage(int page,int size){
        Sort sort = new Sort(Sort.Direction.DESC,"createTime");
        Pageable pageable = new PageRequest(page,size,sort);
        return gymJPA.findAll(pageable);
    }

    public boolean deleteGymByID(Gym gym){
        try {
            gymJPA.delete(gym);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public Gym findGymByID(String gymID){
      return  gymJPA.findByGymID(gymID);
    }

}
