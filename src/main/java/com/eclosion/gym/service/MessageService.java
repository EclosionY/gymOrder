package com.eclosion.gym.service;

import com.eclosion.gym.jpa.MessageJPA;
import com.eclosion.gym.jpa.ReplyJPA;
import com.eclosion.gym.model.entity.Message;
import com.eclosion.gym.model.entity.Reply;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/19
 * Time:14:35
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Service
public class MessageService{
    @Autowired
    private MessageJPA messageJPA;
    @Autowired
    private ReplyJPA replyJPA;

    public Page<Message> findMessagePage(int page,int size){
        Sort sort = new Sort(Sort.Direction.DESC,"createTime");
        Pageable pageable = new PageRequest(page,size,sort);
       return messageJPA.findAll(pageable);
    }

    public String addMessage(Message message){
        try {
            messageJPA.save(message);
        }catch (Exception e){
            return e.toString();
        }

        return "success";
    }

    public Message findMessageByID(String mID){
       return messageJPA.findByMessageID(mID);
    }

    public Boolean saveReply(Reply reply){
        replyJPA.save(reply);
        return true;
    }

    public void removeMsg(Message message){
        messageJPA.delete(message);
    }

}
