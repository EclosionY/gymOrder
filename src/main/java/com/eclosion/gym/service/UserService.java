package com.eclosion.gym.service;

import com.eclosion.gym.jpa.UserJPA;
import com.eclosion.gym.model.entity.User;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/13
 * Time:15:57
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Service
public class UserService{
    @Autowired
    private UserJPA userJPA;


    public User login(User user){
      return userJPA.findByUserNameAndPwd(user.getUserName(),user.getPwd());
    }

    public void register(User user){
        user.setNickName(user.getUserName());
        userJPA.save(user);
    }

    public void saveUser(User user){
        userJPA.save(user);
    }

    public User findUserName(String userName){
       return userJPA.findByUserName(userName);
    }

    public User findUserMail(String mail){
        return userJPA.findByMail(mail);
    }

    public Page<User> findAllUserPage(int page, int size){
        Sort sort = new Sort(Sort.Direction.DESC,"createTime");
        Pageable pageable = new PageRequest(page,size,sort);
        return userJPA.findAll(pageable);
    }
}
