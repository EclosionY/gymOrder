package com.eclosion.gym.model.entity;

import com.eclosion.gym.tools.CreateUUID;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/26
 * Time:14:47
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "t_gym_bookOrder")
public class GymBookOrder {
    @Id
    private String orderID= CreateUUID.getUUID();
    private Date createTime = new Date();

    private int status,bookedNum;

    private String orderDay,orderTimes;

    @ManyToOne
    private User user;

    @ManyToOne
    private  Gym gym;
}
