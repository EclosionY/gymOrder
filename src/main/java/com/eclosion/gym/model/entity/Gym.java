package com.eclosion.gym.model.entity;

import com.eclosion.gym.tools.CreateUUID;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/26
 * Time:14:47
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "t_gym")
public class Gym implements Serializable {
    @Id
    private String gymID= CreateUUID.getUUID();
    private Date createTime = new Date();
    private String gymName,remark,mainImg,gymDes,creator;
    private int num,remainNum,bookedNum=0;

}
