package com.eclosion.gym.model.entity;

import com.eclosion.gym.tools.CreateUUID;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/26
 * Time:15:59
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "t_gym_secondType")
public class GymSecondType {
    @Id
    private String SecondTypeID= CreateUUID.getUUID();
    private Date createTime = new Date();
    private String secondTypeName,remark;

    @ManyToOne(cascade={CascadeType.MERGE,CascadeType.REFRESH},fetch=FetchType.EAGER,optional=false)
    private GymType gymType;

}
