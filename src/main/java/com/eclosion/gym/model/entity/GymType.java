package com.eclosion.gym.model.entity;

import com.eclosion.gym.tools.CreateUUID;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/26
 * Time:15:58
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "t_gym_type")
public class GymType {
    @Id
    private String typeID= CreateUUID.getUUID();
    private Date createTime = new Date();
    private String typeName,remark;
    private String createUser;
    private int status = 0;
    @OneToMany(cascade={CascadeType.REFRESH,CascadeType.MERGE,CascadeType.REMOVE,CascadeType.PERSIST},fetch=FetchType.LAZY,mappedBy="gymType")
    private List<GymSecondType> gymSecondTypes;
}
