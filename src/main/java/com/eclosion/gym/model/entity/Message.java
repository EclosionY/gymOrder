package com.eclosion.gym.model.entity;


import com.eclosion.gym.tools.CreateUUID;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/16
 * Time:21:43
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "t_message")
public class Message implements Serializable{

    @Id
    private String messageID= CreateUUID.getUUID();
    private String content,remark,ip,position;

    @ManyToOne(cascade={CascadeType.MERGE,CascadeType.REFRESH},fetch=FetchType.EAGER,optional=false)
    @JoinColumn(name="userID")
    private User user;
    private Date createTime = new Date();

    @OneToMany(cascade={CascadeType.REFRESH,CascadeType.MERGE,CascadeType.REMOVE,CascadeType.PERSIST},fetch=FetchType.EAGER,mappedBy="message")
    private List<Reply> replies;

}
