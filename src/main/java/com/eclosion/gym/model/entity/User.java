package com.eclosion.gym.model.entity;

import com.eclosion.gym.tools.CreateUUID;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/13
 * Time:15:10
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "t_user")
public class User implements Serializable{
    @Id
    private String userID=CreateUUID.getUUID();
    @Column(unique = true)
    private String userName;
    private String pwd,stuNo,Role="0",headPath="libs/img/defaultHead/dhead02.jpg",remark,nickName,mail,tel,code,realName;
    private Integer age;
    private Date createTime = new Date();
    private int status;
    @OneToMany(cascade={CascadeType.REFRESH,CascadeType.MERGE,CascadeType.REMOVE,CascadeType.PERSIST},fetch=FetchType.LAZY,mappedBy="user")
    private List<Message> messages;

    @OneToMany
    private List<GymBookOrder> gymBookOrders;

    public String getUserID() {
        return userID;
    }
}
