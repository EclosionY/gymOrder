package com.eclosion.gym.model.entity;

import com.eclosion.gym.tools.CreateUUID;
import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/26
 * Time:13:58
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "t_replies")
public class Reply {
    @Id
    private String replyID= CreateUUID.getUUID();
    private String content,remark,ip,position;

    @ManyToOne
    @JoinColumn(name="userID")
    private User user;
    private Date createTime = new Date();

    @ManyToOne(cascade={CascadeType.MERGE,CascadeType.REFRESH},fetch=FetchType.EAGER,optional=false)
    @JoinColumn(name = "message_id")
    private Message message;
}
