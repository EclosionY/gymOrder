package com.eclosion.gym.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/16
 * Time:14:26
 */

@Controller
@AllArgsConstructor
@Getter
@Setter
public class CenterController {
    //负责处理请求返回对应的页面来完成ajax请求;
    @RequestMapping("/re")
    public String login(@RequestParam(value="page",required=true)  String page,Model model){
        return page;
    }

}
