package com.eclosion.gym.controller;

import com.eclosion.gym.model.entity.Message;
import com.eclosion.gym.model.entity.Reply;
import com.eclosion.gym.model.entity.User;
import com.eclosion.gym.service.MessageService;
import com.eclosion.gym.service.UserService;
import com.eclosion.gym.tools.IPtools;
import com.eclosion.gym.tools.QueryIPtools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/19
 * Time:14:58
 */

@Controller
public class MessageController {
    @Autowired
    private MessageService messageService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/addMessage",method = RequestMethod.POST)
    @ResponseBody
    public String addMessage(@RequestBody Message message, HttpServletRequest request){
        try {
        message.setUser((User) request.getSession().getAttribute("loginUser"));
        message.setIp(IPtools.getIpAddr(request));
        message.setPosition(QueryIPtools.queryIP(IPtools.getIpAddr(request)));
        messageService.addMessage(message);
        }catch (Exception e){
            return e.toString();
        }
        return "success";
    }

    @RequestMapping(value = "/findMessagePage",method = RequestMethod.GET)
    public String findMessagePage(@RequestParam(value="pageNum",required=true)int pageNum, @RequestParam(value="pageSize",required=false)int pageSize,@RequestParam(value="type",required=true)int type, Model model){
        Page<Message> messages = messageService.findMessagePage(pageNum,pageSize);
        model.addAttribute("messageData",messages);
        model.addAttribute("total",messages.getTotalElements());
        model.addAttribute("size",messages.getSize());
        model.addAttribute("pageNum",pageNum);
        if(type==0){
            return "message";
        }else {
            return "adminCenter/messageList";
        }

    }

    @RequestMapping(value = "/addReply",method = RequestMethod.POST)
    @ResponseBody
    public String addReply(@RequestBody Reply reply, HttpServletRequest request){
        try {
            reply.setUser((User) request.getSession().getAttribute("loginUser"));
            reply.setIp(IPtools.getIpAddr(request));
            reply.setPosition(QueryIPtools.queryIP(IPtools.getIpAddr(request)));
            reply.setMessage(messageService.findMessageByID(reply.getRemark()));
            messageService.saveReply(reply);
        }catch (Exception e){
            return e.toString();
        }
        return "success";
    }

    @RequestMapping(value = "/deleteMsg",method = RequestMethod.DELETE)
    @ResponseBody
    public boolean deleteMsg(@RequestBody Message message){
        try {
            messageService.removeMsg(message);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

    }
}
