package com.eclosion.gym.controller;

import com.eclosion.gym.model.entity.Gym;
import com.eclosion.gym.model.entity.GymBookOrder;
import com.eclosion.gym.model.entity.User;
import com.eclosion.gym.service.GymOrderService;
import com.eclosion.gym.service.GymService;
import org.beetl.core.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller
public class gymOrderController {
    private final GymOrderService gymOrderService;
    private final GymService gymService;
    @Autowired
    public gymOrderController(GymOrderService gymOrderService,GymService gymService) {
        this.gymOrderService = gymOrderService;
        this.gymService = gymService;
    }

    @RequestMapping(value = "/addGymOrder",method = RequestMethod.POST)
    @ResponseBody
    public boolean addGymOrder(@RequestBody GymBookOrder gymBookOrder, HttpServletRequest request){
        try {
            gymBookOrder.setUser((User) request.getSession().getAttribute("loginUser"));
            gymBookOrder.setOrderTimes(gymBookOrder.getOrderTimes().replace("\n",""));
            gymOrderService.addGymOrder(gymBookOrder);
            Gym gym = gymService.findGymByID(gymBookOrder.getGym().getGymID());
            gym.setBookedNum(gym.getBookedNum()+gymBookOrder.getBookedNum());
            gymService.addGym(gym);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @RequestMapping(value = "/findMyOrders",method = RequestMethod.GET)
    public String findMyOrders(@RequestParam(value="pageNum",required=true)int pageNum, @RequestParam(value="pageSize",required=false)int pageSize,Model model,HttpServletRequest request){
        User user = (User) request.getSession().getAttribute("loginUser");
        Page<GymBookOrder> gymBookOrders = gymOrderService.findMyBookOrder(user,pageNum,pageSize,3);
        model.addAttribute("gymOrders",gymBookOrders);
        model.addAttribute("total",gymBookOrders.getTotalElements());
        model.addAttribute("size",gymBookOrders.getSize());
        model.addAttribute("pageNum",pageNum);
        return "personCenter/myBook";
    }

    @RequestMapping(value = "/findAllOrders",method = RequestMethod.GET)
    public String findAllOrders(@RequestParam(value="pageNum",required=true)int pageNum, @RequestParam(value="pageSize",required=false)int pageSize,Model model,HttpServletRequest request){
        Page<GymBookOrder> gymBookOrders = gymOrderService.findAllBookOrder(pageNum,pageSize);
        model.addAttribute("allGymOrders",gymBookOrders);
        model.addAttribute("total",gymBookOrders.getTotalElements());
        model.addAttribute("size",gymBookOrders.getSize());
        model.addAttribute("pageNum",pageNum);
        return "adminCenter/bookedList";
    }

    //需要场地,日期，人数，返回该日期各时段的预订情况，人数
    @RequestMapping(value = "/initBooked",method = RequestMethod.POST)
    public String initBookedPage(@RequestBody GymBookOrder gymBookOrder,Model model){
        Gym gym = gymService.findGymByID(gymBookOrder.getGym().getGymID());
        List<GymBookOrder> gymBookOrders = gymOrderService.findGymOrderByDayAndStatusAndGym(gymBookOrder.getOrderDay(),0,gym);
        Map<String,Integer> bookStatus = new LinkedHashMap<>();
        for (int i = 8;i < 22;i++){
            bookStatus.put(i+":00-"+(i+1)+":00",0);
        }

        if(!gymBookOrders.isEmpty()){
            for(GymBookOrder gymBookOrder1 : gymBookOrders){
                String[] times = gymBookOrder1.getOrderTimes().split(",");
                for (String time : times){
                    bookStatus.put(time,bookStatus.get(time)+gymBookOrder1.getBookedNum());
                }
            }
        }

        model.addAttribute("maxNum",gym.getNum());
        model.addAttribute("bookStatus",bookStatus);
        model.addAttribute("reNum",gymBookOrder.getBookedNum());

        return "common/chooseTime";
    }

    @RequestMapping(value = "/cancelBook",method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean cancelBook(@RequestBody GymBookOrder gymBookOrder){
        try {
            gymOrderService.deleteGymOrder(gymBookOrder);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

    }

}
