package com.eclosion.gym.controller;


import com.eclosion.gym.model.entity.Gym;
import com.eclosion.gym.model.entity.User;
import com.eclosion.gym.service.GymService;
import com.eclosion.gym.tools.CreateUUID;
import com.eclosion.gym.tools.FileUtil;
import com.eclosion.gym.tools.ResponseMap;
import com.eclosion.gym.tools.SystemType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
@AllArgsConstructor
@Getter
@Setter
public class gymController {
    private GymService gymService;

    @RequestMapping(value = "/getGyms",method = RequestMethod.GET)
    public String getGyms(Model model){
        model.addAttribute("gymList",gymService.findGymList());
        return  "common/chooseDateTools";
    }

    @RequestMapping(value = "/findGymPage",method = RequestMethod.GET)
    public String findGymPage(@RequestParam(value="pageNum",required=true)int pageNum, @RequestParam(value="pageSize",required=false)int pageSize, Model model){
        Page<Gym> gymPage = gymService.findGymPage(pageNum,pageSize);
        model.addAttribute("gymPage",gymPage);
        model.addAttribute("total",gymPage.getTotalElements());
        model.addAttribute("size",gymPage.getSize());
        model.addAttribute("pageNum",pageNum);
        return "adminCenter/gymList";
    }

    @RequestMapping(value = "/addGym",method = RequestMethod.POST)
    @ResponseBody
    public ResponseMap addGym(HttpServletRequest request, @RequestParam(value = "file", required = false) MultipartFile file,
                              Gym gym){
        String contentType = file.getContentType();
        String fileName = file.getOriginalFilename();
        String fileTargetPath = ":8082/gym/";
        String winFilePath = "E:/fileUpload/gym/";
        String linuxFilePath = "/home/fileUpload/gym/";
        String filePath="";
        ResponseMap responseMap = new ResponseMap();
        if (SystemType.isWin()){
            filePath = winFilePath;
        }else {
            filePath = linuxFilePath;
        }

        try {

            fileName = CreateUUID.getUUID()+"_g.jpg";
            if(!file.isEmpty()){
                FileUtil.uploadFile(file.getBytes(), filePath, fileName);
                gym.setMainImg(fileTargetPath+fileName);
            }

            gym.setRemainNum(gym.getNum());
            User user = (User) request.getSession().getAttribute("loginUser");
            gym.setCreator(user.getUserName());
            gymService.addGym(gym);
        } catch (Exception e) {
            e.printStackTrace();
            responseMap.err("文件上传错误");
            return responseMap;
        }
        //返回json

        responseMap.ok();
        return responseMap;
    }

    @RequestMapping(value = "/deleteGym",method = RequestMethod.DELETE)
    @ResponseBody
    public Map<String, Boolean> deleteGym(@RequestBody Gym gym){
        HashMap<String, Boolean> map = new HashMap<>();
        try {
            gymService.deleteGymByID(gym);
            map.put("success",true);
            return map;
        }catch (Exception e){
            e.printStackTrace();
            map.put("success",false);
            return map;
        }
    }

    @RequestMapping(value = "/findGymByID",method = RequestMethod.GET)
    public String findGynByID (@RequestParam(value="gymID",required=true)String gymID, Model model){
        model.addAttribute("gym",gymService.findGymByID(gymID));
        return "adminCenter/checkGym";
    }
}
