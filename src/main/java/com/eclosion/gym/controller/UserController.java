package com.eclosion.gym.controller;

import com.eclosion.gym.model.entity.User;
import com.eclosion.gym.service.MailService;
import com.eclosion.gym.service.UserService;
import com.eclosion.gym.tools.CreateUUID;
import com.eclosion.gym.tools.FileUtil;
import com.eclosion.gym.tools.IPtools;
import com.eclosion.gym.tools.SystemType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/21
 * Time:11:58
 */

@Controller
@Getter
@Setter
public class UserController {
    private final UserService userService;
    private final MailService mailService;

    @Autowired
    public UserController(UserService userService, MailService mailService) {
        this.userService = userService;
        this.mailService = mailService;
    }

    @RequestMapping(value="/changeHeadPath", method = RequestMethod.POST)
    public @ResponseBody String uploadImg(@RequestParam("file") MultipartFile file, HttpServletRequest request) {
        String contentType = file.getContentType();
        String fileName = file.getOriginalFilename();
        String fileTargetPath = ":8082/head/";
        String winFilePath = "E:/fileUpload/head/";
        String linuxFilePath = "/home/fileUpload/head/";
        String filePath="";
        if (SystemType.isWin()){
            filePath = winFilePath;
        }else {
            filePath = linuxFilePath;
        }

        try {
            User loginUser = (User) request.getSession().getAttribute("loginUser");

            if(loginUser.getHeadPath().contains(":8082/head/")){
                FileUtil.delete(filePath+loginUser.getHeadPath().split("/")[2]);
            }

            fileName = CreateUUID.getUUID()+"_h.jpg";
            FileUtil.uploadFile(file.getBytes(), filePath, fileName);
            loginUser.setHeadPath(fileTargetPath+fileName);
            userService.saveUser(loginUser);
            request.getSession().setAttribute("loginUser",loginUser);
        } catch (Exception e) {
            e.printStackTrace();
            return "文件上传错误!";
        }
        //返回json
        return fileTargetPath+fileName;
    }

    @RequestMapping(value = "/changeInfo",method = RequestMethod.POST)
    @ResponseBody
    public String changeInfo(@RequestBody User user, HttpServletRequest request){
        User loginUser = (User) request.getSession().getAttribute("loginUser");
        loginUser.setNickName(user.getNickName());
        if(!user.getMail().equals(loginUser.getMail())){loginUser.setStatus(0);}
        loginUser.setMail(user.getMail());
        loginUser.setRealName(user.getRealName());
        loginUser.setTel(user.getTel());
        request.getSession().setAttribute("loginUser",loginUser);
        userService.saveUser(loginUser);
        return "ok";
    }

    @RequestMapping(value = "/changePwd",method = RequestMethod.POST)
    @ResponseBody
    public boolean changePwd(@RequestBody User user, HttpServletRequest request){
        User loginUser = (User) request.getSession().getAttribute("loginUser");
        if (user.getPwd().equals(loginUser.getPwd())){
            loginUser.setPwd(user.getRemark());
            userService.saveUser(loginUser);
            return true;
        }else {
            return false;
        }
    }

    @RequestMapping(value = "/mailSender",method = RequestMethod.POST)
    @ResponseBody
    public boolean mailSender(HttpServletRequest request){
          User loginUser = (User)request.getSession().getAttribute("loginUser");
          String mail = loginUser.getMail();
          String uCode = "";
        try {
            uCode =  mailService.sendCodeMail(mail);
            loginUser.setCode(uCode);
            request.getSession().setAttribute("loginUser",loginUser);
            userService.saveUser(loginUser);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @RequestMapping(value = "/mailCheck",method = RequestMethod.POST)
    @ResponseBody
    public boolean mailCheck(@RequestBody User user, HttpServletRequest request){
        User loginUser = (User)request.getSession().getAttribute("loginUser");
        if(user.getCode().equals(loginUser.getCode())){
            loginUser.setStatus(1);
            request.getSession().setAttribute("loginUser",loginUser);
            userService.saveUser(loginUser);
            return true;
        }else {
            return false;
        }
    }

    @RequestMapping(value = "/findUserPage",method = RequestMethod.GET)
    public String findAllOrders(@RequestParam(value="pageNum",required=true)int pageNum, @RequestParam(value="pageSize",required=false)int pageSize,Model model){
        Page<User> userPage = userService.findAllUserPage(pageNum,pageSize);
        model.addAttribute("userPage",userPage);
        model.addAttribute("total",userPage.getTotalElements());
        model.addAttribute("size",userPage.getSize());
        model.addAttribute("pageNum",pageNum);
        return "adminCenter/userList";
    }

}
