package com.eclosion.gym.controller;
import com.eclosion.gym.model.entity.User;
import com.eclosion.gym.service.MailService;
import com.eclosion.gym.service.UserService;
import com.eclosion.gym.tools.IPtools;
import com.eclosion.gym.tools.SystemType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/13
 * Time:10:10
 */
@Controller
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LoginController {
    @Autowired
    private UserService userService;
    @Autowired
    private MailService mailService;

    @RequestMapping(value = "/index",method = RequestMethod.GET)
    public String showIndex(@ModelAttribute(value = "user") User user, HttpServletRequest request){
        return "index";
    }

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String index(){
        return "redirect:/login";
    }

    @ResponseBody
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    public String login(@RequestBody User user, HttpServletRequest request){
        User loginUser =  userService.login(user);
       if (loginUser!=null){
           request.getSession().setAttribute("loginUser",loginUser);

           if (SystemType.isWin()){
               request.getSession().setAttribute("myIP",IPtools.getLocalIP());
           }else {
               request.getSession().setAttribute("myIP","eclosiony.top");
           }

           return "index";
       }
       else {
           return "用户名或密码错误，请检查输入";
       }
    }

    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public String loginPage(User user, HttpServletRequest request, Model model){
        return "login";
    }


    @RequestMapping(value = "/register",method = RequestMethod.POST)
    @ResponseBody
    public Map<Object,Object> register(@RequestBody User user){
        Boolean regFlag = true;
        try {
            user.setNickName(user.getUserName());
            userService.register(user);
        }catch (Exception e){
            regFlag=false;
        }

        Map<Object,Object> map = new HashMap<Object,Object>();
        map.put("success",regFlag);
        return map;
    }

    @RequestMapping(value = "/loguot",method = RequestMethod.GET)
    public String logout(@ModelAttribute(value = "user") User user, HttpServletRequest request){
        request.getSession().removeAttribute("loginUser");
        return "redirect:/login";
    }

    @RequestMapping(value = "/findPwd",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> findPwd(@RequestBody User user) throws Exception {
        Map<String,Object> res = new HashMap<>();
        User finUser = userService.findUserName(user.getUserName());
        if (finUser==null){
            res.put("success",false);
            res.put("position","usrErr");
            res.put("reason","不存在该用户");
        }else {
            if(!user.getMail().equals(finUser.getMail())){
                res.put("success",false);
                res.put("position","mailErr");
                res.put("reason","绑定邮箱错误");
            }else {
                res.put("success",mailService.sendPwdMail(finUser.getMail(),finUser.getUserName(),finUser.getPwd()));
            }
        }
        return res;
    }
}
