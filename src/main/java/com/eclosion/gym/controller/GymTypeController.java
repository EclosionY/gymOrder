package com.eclosion.gym.controller;

import com.eclosion.gym.model.entity.GymType;
import com.eclosion.gym.service.GymTypeService;
import com.eclosion.gym.tools.ResponseMap;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/27
 * Time:15:13
 */

@Getter
@Setter
@Controller
public class GymTypeController {

    private final GymTypeService gymTypeService;

    @Autowired
    public GymTypeController(GymTypeService gymTypeService) {
        this.gymTypeService = gymTypeService;
    }

    @RequestMapping(value = "/addGymType",method = RequestMethod.POST)
    @ResponseBody
    public Map addGymType(@RequestBody GymType gymType){
        gymTypeService.addGymType(gymType);
        return new ResponseMap().ok();
    }

    @RequestMapping(value = "/findGymTypePage",method = RequestMethod.GET)
    public String findGymTypePage(@RequestParam(value="pageNum",required=true)int pageNum, @RequestParam(value="pageSize",required=false)int pageSize, Model model){
        Page<GymType> gymTypes = gymTypeService.findGymTypePage(pageNum,pageSize);
        model.addAttribute("gymTypes",gymTypes);
        model.addAttribute("total",gymTypes.getTotalElements());
        model.addAttribute("size",gymTypes.getSize());
        return "adminCenter/typeList";
    }

    @RequestMapping(value = "/deleteGymType",method = RequestMethod.POST)
    @ResponseBody
    public Map deleteGymType(@RequestBody GymType gymType){
        gymTypeService.deleteGymType(gymType);
        return new ResponseMap().ok();
    }

}
