package com.eclosion.gym.test;

import com.eclosion.gym.model.entity.GymType;
import com.eclosion.gym.service.GymTypeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created with IDEA
 * author:zym
 * Date:2018/3/26
 * Time:11:21
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class myTest {
    @Autowired
    GymTypeService gymTypeService;
    @Test
    public void reply(){
        GymType gymType = new GymType();
        gymType.setTypeID("898cd416988e4ba995993f2d89a2db2c");

        gymTypeService.deleteGymType(gymType);
    }
}
